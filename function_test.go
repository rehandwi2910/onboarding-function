package f_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	f "stockbit.com/onboarding-function"
	"testing"
)

func TestEmployees(t *testing.T) {
	body := f.GaEmployeeAddRequest{}
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(body)
	if err != nil {
		log.Fatal(err)
	}
	w := httptest.NewRecorder()

	req := httptest.NewRequest(http.MethodPost, "/Employees", bytes.NewReader(buf.Bytes()))
	f.Employees(w, req)

	res := w.Result()
	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nul got %v", err)
	}

	log.Println(string(data))
}
