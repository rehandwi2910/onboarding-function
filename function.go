package f

import (
	"cloud.google.com/go/pubsub"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type GaEmployeeAddRequest struct {
	Email                        string   `json:"email"`
	FullName                     string   `json:"full_name"`
	PlaceOfBirth                 string   `json:"place_of_birth"`
	DateOfBirth                  string   `json:"date_of_birth"`
	Gender                       string   `json:"gender"`
	MaritalStatus                string   `json:"marital_status"`
	Religion                     string   `json:"religion"`
	AddressInKtp                 string   `json:"address_in_ktp"`
	AddressInDomicile            string   `json:"address_in_domicile"`
	PhoneNumber                  string   `json:"phone_number"`
	SocialMedia                  string   `json:"social_media"`
	PhotoUrl                     []string `json:"photo_url"`
	Certifications               []string `json:"certifications"`
	CertificateDocuments         []string `json:"certificate_documents"`
	LastEducation                string   `json:"last_education"`
	LastCertificateUrl           []string `json:"last_certificate_url"`
	LastEducationFrom            string   `json:"last_education_from"`
	LastPlaceOfWorkFirst         string   `json:"last_place_of_work_first"`
	LastPlaceOfWorkSecond        string   `json:"last_place_of_work_second"`
	BankAccountNumber            string   `json:"bank_account_number"`
	BankAccountName              string   `json:"bank_account_name"`
	BankAccountScan              []string `json:"bank_account_scan"`
	BpjsHealthNumber             string   `json:"bpjs_health_number" `
	BpjsHealthScan               []string `json:"bpjs_health_scan"`
	BpjsEmploymentNumber         string   `json:"bpjs_employment_number"`
	BpjsEmploymentScan           []string `json:"bpjs_employment_scan"`
	EmergencyContactName         string   `json:"emergency_contact_name"`
	EmergencyContactRelationship string   `json:"emergency_contact_relationship"`
	EmergencyContactNumber       string   `json:"emergency_contact_number"`
	CoupleName                   string   `json:"couple_name"`
	RelationshipStatus           string   `json:"relationship_status"`
	RelationshipDateOfBirth      string   `json:"relationship_date_of_birth"`
	ChildrenName                 string   `json:"children_name"`
	FirstChildName               string   `json:"first_child_name"`
	FirstChildDOB                string   `json:"first_child_dob"`
	SecondChildName              string   `json:"second_child_name"`
	SecondChildDOB               string   `json:"second_child_dob"`
	ThirdChildName               string   `json:"third_child_name"`
	ThirdChildDOB                string   `json:"third_child_dob"`
	FamilyCardScan               []string `json:"family_card_scan"`
	OnboardingEquipment          string   `json:"onboarding_equipment"`
	Description                  string   `json:"description"`
}

func Employees(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(`{"message": "not found"}`))
	}

	req := GaEmployeeAddRequest{}

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte(`{"message": "can't marshall request'"}`))
	}

	b, err := json.Marshal(req)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		w.Write([]byte(`{"message": "can't marshall request'"}`))
	}

	err = publishMsg("stockbit-productivity-engineer", "onboarding-employee", b)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{"message": "internal server error'"}`))
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message": "success'"}`))
}

func publishMsg(projectID, topicID string, msg []byte) (err error) {
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		log.Println(err)
		return err
	}
	defer client.Close()

	t := client.Topic(topicID)
	result := t.Publish(
		ctx, &pubsub.Message{
			Data: msg,
		},
	)

	id, err := result.Get(ctx)
	if err != nil {
		log.Println(err)
		return err
	}

	fmt.Printf("Published a message; msg ID: %v\n", id)
	return nil
}
